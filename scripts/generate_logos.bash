#!/bin/bash

# THIS SCRIPT REQUIRES INKSCAPE INSTALLATION ON DEBIAN BASED DISTROS

inkscape -w 16 -h 16 assets/logo.svg --export-filename extension/assets/icon16.png
inkscape -w 32 -h 32 assets/logo.svg --export-filename extension/assets/icon32.png
inkscape -w 48 -h 48 assets/logo.svg --export-filename extension/assets/icon48.png
inkscape -w 128 -h 128 assets/logo.svg --export-filename extension/assets/icon128.png
inkscape -w 256 -h 256 assets/logo.svg --export-filename extension/assets/icon256.png
