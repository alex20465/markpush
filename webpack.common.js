const path = require("path");
const FileManagerPlugin = require('filemanager-webpack-plugin');

module.exports = {
  entry: {

    // Extension
    popup: path.join(__dirname, "src/extension/popup.tsx"),
    popupBackground: path.join(__dirname, "src/extension/background.ts"),
  },
  output: {
    path: path.join(__dirname, "build"),
    filename: "[name].js"
  },

  plugins: [
    new FileManagerPlugin({
      events: {
        onEnd: {
          copy: [
            { source: 'build/popup.js', destination: 'extension/build/popup.js' },
            { source: 'build/popupBackground.js', destination: 'extension/build/background.js' },
          ],
          archive: [
            {
              source: "extension/", destination: 'extension.zip', format: "zip"
            }
          ]
        },
      }
    })
  ],
  module: {
    rules: [
      {
        exclude: /node_modules/,
        test: /\.tsx?$/,
        use: "ts-loader"
      },

      {
        test: /\.less$/,
        use: [
          {
            loader: "style-loader"
          },
          {
            loader: "css-loader"
          },
          {
            loader: "less-loader",
            options: {
              lessOptions: {
                javascriptEnabled: true
              }
            }
          }
        ]
      }
    ]
  },
  resolve: {
    fallback: { "path": false },
    extensions: [".ts", ".tsx", ".js"]
  }
};
