# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.4](https://gitlab.com/alex20465/markpush/compare/v0.0.3...v0.0.4) (2021-06-01)


### Features

* add article title as markdown header title on top ([f625a63](https://gitlab.com/alex20465/markpush/commit/f625a634fcc7890aceda2098541ba2ea5a40a0fe))
* Allow user to specify local-image downloading ([e1064db](https://gitlab.com/alex20465/markpush/commit/e1064db6acfc510051dcfabb124978e609774c0b))

### 0.0.3 (2020-12-10)


### Features

* add footer and error page ([b887302](https://gitlab.com/alex20465/articlipper/commit/b88730234b6fe767f04e56267fc9bf287c7103b3))
* add LICENSE file and change package name and description ([e8984f2](https://gitlab.com/alex20465/articlipper/commit/e8984f2fcc8092c1564564ba10f82ce26001e72d))
* add location specfication ([aa59dfe](https://gitlab.com/alex20465/articlipper/commit/aa59dfe2ebe1fdbb0150e34a57ff33fa9576e036))
* add logo ([e37ebf9](https://gitlab.com/alex20465/articlipper/commit/e37ebf9a69953be7c9a6422af0704ef2eb183e59))
* add providers: gitlab, github, bitbucket ([13a47cd](https://gitlab.com/alex20465/articlipper/commit/13a47cd67243ba071cd7372f1c7646ebbb802e16))
* add repository status messages onRepositoryList ([bf86763](https://gitlab.com/alex20465/articlipper/commit/bf867630ba8d8975fd2f5d8a6d44facb2299a915))
* archive extension on webpack end event ([c868810](https://gitlab.com/alex20465/articlipper/commit/c868810fd4bd294f766ab829ce1c3f2c67a0485f))
* change reamde.md ([238066b](https://gitlab.com/alex20465/articlipper/commit/238066b519614d307d0f5e67ec86ae4227d027f2))
* Clipper getting started setup result page (when no repository is configured) ([6172126](https://gitlab.com/alex20465/articlipper/commit/6172126bfdbcb166a0aed779dd374f7cdcbfa350))
* configure standard-version to bump the manifest.json file ([fdf4bc8](https://gitlab.com/alex20465/articlipper/commit/fdf4bc853c8e5a5ed3aedde89e6d2b1019795969))
* define gitlab contribution link ([1921ab4](https://gitlab.com/alex20465/articlipper/commit/1921ab441eb9ab72195ad939b78155e4f7f97256))
* Implement clipper form skeleton while fetching article ([f6119ad](https://gitlab.com/alex20465/articlipper/commit/f6119adbe9821a1ae9fb1d8f4ac23f61ceb6ff3b))
* implement repository delete ([51cc7e4](https://gitlab.com/alex20465/articlipper/commit/51cc7e4f09b8306f5ef18afe25631f2a6dab8836))
* Implement script to generate logo.svg into different pngs ([9402bb5](https://gitlab.com/alex20465/articlipper/commit/9402bb5cb7975f8a6e67966de4095eb46dd31152))
* improve general UX with loading indicators and notifications ([b1a745f](https://gitlab.com/alex20465/articlipper/commit/b1a745f3a8928b485f9a0e0936147859a8554365))
* option to clone after save ([6ca3019](https://gitlab.com/alex20465/articlipper/commit/6ca301996eea1f08073de04252194a7d6c94f55f))
* replace incorrect relative document image-sources ([ce28c09](https://gitlab.com/alex20465/articlipper/commit/ce28c09fc366fb62499981f5271b4f405bdd7dc3))
* setup gitlab-ci.yml (CI) build ([5de04d0](https://gitlab.com/alex20465/articlipper/commit/5de04d0dc39ca14798029c30df0be25b02310fba))
* Show notification on repository-create and redirect to repository-list ([1f273bf](https://gitlab.com/alex20465/articlipper/commit/1f273bf2a2f186ede0b0e5d4be68d0807b10562b))
* Support every browser with the 'browser' API support such as Firefox ([d90bcde](https://gitlab.com/alex20465/articlipper/commit/d90bcdee74996cb929ea0caf2d44ef927cf944f3))
* theming ([9778283](https://gitlab.com/alex20465/articlipper/commit/97782832ba7f6ce662329dd2453f5f7650cd119e))
* theming ([009dc39](https://gitlab.com/alex20465/articlipper/commit/009dc39d34d33c14756d0c5a81e06314e171020a))
* Update icons ([cc11ab3](https://gitlab.com/alex20465/articlipper/commit/cc11ab3e33f5344f7ad7b8658fa962f36ed17580))
* update manifest.json ([e05d2cf](https://gitlab.com/alex20465/articlipper/commit/e05d2cfc30889808b88bb94c1ca10198496632eb))


### Bug Fixes

* Error while saving article in an existing directory ([ccac2f1](https://gitlab.com/alex20465/articlipper/commit/ccac2f1a3a7e179caf35beaa4391e6e038e051ee))
* first clone leads to 'ENOENT' ([6f6e626](https://gitlab.com/alex20465/articlipper/commit/6f6e6263c92e1575550e7724f20d03fe074bf04e))
* Fix broken grouped buttons on repository-list component ([1041266](https://gitlab.com/alex20465/articlipper/commit/1041266985c4ede28c625a8a2bd2ca0930765a03))
* missing case for repository initialization if the repository direction does not exist ([124a11e](https://gitlab.com/alex20465/articlipper/commit/124a11e1558f889a4fd5c37a242e2ead5738c90d))
* Remove not implemented preview botton ([4d148ed](https://gitlab.com/alex20465/articlipper/commit/4d148edd8feda92f6609b75c9652da8124dc4944))
* RepositoryCreateForm initial values ([02f0a77](https://gitlab.com/alex20465/articlipper/commit/02f0a7789bf108db5f80831151daf4db74e456cb))
* update loading state on save/cloning (repository-edit view) ([b39510e](https://gitlab.com/alex20465/articlipper/commit/b39510eeea862d6f6234658986b43e1bad38d357))
* update loading text ([6b6011f](https://gitlab.com/alex20465/articlipper/commit/6b6011f2eb24d7afbb742626163a303c68a1afb6))

### 0.0.2 (2020-12-06)


### Features

* add footer and error page ([b887302](https://gitlab.com/alex20465/articlipper/commit/b88730234b6fe767f04e56267fc9bf287c7103b3))
* add LICENSE file and change package name and description ([e8984f2](https://gitlab.com/alex20465/articlipper/commit/e8984f2fcc8092c1564564ba10f82ce26001e72d))
* add location specfication ([aa59dfe](https://gitlab.com/alex20465/articlipper/commit/aa59dfe2ebe1fdbb0150e34a57ff33fa9576e036))
* add logo ([e37ebf9](https://gitlab.com/alex20465/articlipper/commit/e37ebf9a69953be7c9a6422af0704ef2eb183e59))
* add repository status messages onRepositoryList ([bf86763](https://gitlab.com/alex20465/articlipper/commit/bf867630ba8d8975fd2f5d8a6d44facb2299a915))
* change reamde.md ([238066b](https://gitlab.com/alex20465/articlipper/commit/238066b519614d307d0f5e67ec86ae4227d027f2))
* define gitlab contribution link ([1921ab4](https://gitlab.com/alex20465/articlipper/commit/1921ab441eb9ab72195ad939b78155e4f7f97256))
* Implement clipper form skeleton while fetching article ([f6119ad](https://gitlab.com/alex20465/articlipper/commit/f6119adbe9821a1ae9fb1d8f4ac23f61ceb6ff3b))
* implement repository delete ([51cc7e4](https://gitlab.com/alex20465/articlipper/commit/51cc7e4f09b8306f5ef18afe25631f2a6dab8836))
* Implement script to generate logo.svg into different pngs ([9402bb5](https://gitlab.com/alex20465/articlipper/commit/9402bb5cb7975f8a6e67966de4095eb46dd31152))
* improve general UX with loading indicators and notifications ([b1a745f](https://gitlab.com/alex20465/articlipper/commit/b1a745f3a8928b485f9a0e0936147859a8554365))
* option to clone after save ([6ca3019](https://gitlab.com/alex20465/articlipper/commit/6ca301996eea1f08073de04252194a7d6c94f55f))
* replace incorrect relative document image-sources ([ce28c09](https://gitlab.com/alex20465/articlipper/commit/ce28c09fc366fb62499981f5271b4f405bdd7dc3))
* Show notification on repository-create and redirect to repository-list ([1f273bf](https://gitlab.com/alex20465/articlipper/commit/1f273bf2a2f186ede0b0e5d4be68d0807b10562b))
* theming ([009dc39](https://gitlab.com/alex20465/articlipper/commit/009dc39d34d33c14756d0c5a81e06314e171020a))
* theming ([9778283](https://gitlab.com/alex20465/articlipper/commit/97782832ba7f6ce662329dd2453f5f7650cd119e))
* Update icons ([cc11ab3](https://gitlab.com/alex20465/articlipper/commit/cc11ab3e33f5344f7ad7b8658fa962f36ed17580))
* update manifest.json ([e05d2cf](https://gitlab.com/alex20465/articlipper/commit/e05d2cfc30889808b88bb94c1ca10198496632eb))


### Bug Fixes

* Error while saving article in an existing directory ([ccac2f1](https://gitlab.com/alex20465/articlipper/commit/ccac2f1a3a7e179caf35beaa4391e6e038e051ee))
* Fix broken grouped buttons on repository-list component ([1041266](https://gitlab.com/alex20465/articlipper/commit/1041266985c4ede28c625a8a2bd2ca0930765a03))
* Remove not implemented preview botton ([4d148ed](https://gitlab.com/alex20465/articlipper/commit/4d148edd8feda92f6609b75c9652da8124dc4944))
* RepositoryCreateForm initial values ([02f0a77](https://gitlab.com/alex20465/articlipper/commit/02f0a7789bf108db5f80831151daf4db74e456cb))
* update loading state on save/cloning (repository-edit view) ([b39510e](https://gitlab.com/alex20465/articlipper/commit/b39510eeea862d6f6234658986b43e1bad38d357))
* update loading text ([6b6011f](https://gitlab.com/alex20465/articlipper/commit/6b6011f2eb24d7afbb742626163a303c68a1afb6))
