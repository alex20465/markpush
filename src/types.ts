export enum RepositoryProvider {
    GITEA = "gitea",
    GITLAB = "gitlab",
    GITHUB = "github",
    BITBUCKET = "bitbucket",
}

export enum RepositoryStatus {
    DEFINED = "DEFINED",
    SYNC = "SYNC",
    OUT_SYNC = "OUT_SYNC",
    ERROR = "ERROR",
}
export interface RepositoryAttributes {
    name: string
    url: string
    accessToken: string
    location: string
    downloadImages: boolean
    titleHeader: boolean
    provider: RepositoryProvider
    status?: RepositoryStatus
}

export interface Repository {
    id: string
    attributes: RepositoryAttributes
}

export interface ArticleImage {
    id: string
    dest: string
    sourceUrl: string
}

export interface Article {
    title: string
    source: string
    content: string
    images: ArticleImage[]
}