import { Schema } from "@orbit/data";
import Database from "@orbit/indexeddb";
import Memory from "@orbit/memory";
import Coordinator, { SyncStrategy } from "@orbit/coordinator";

export function init() {

    const schema = new Schema({
        models: {
            repository: {
                attributes: {
                    name: { type: "string" },
                    url: { type: "string" },
                    location: { type: "string" },
                    downloadImages: { type: "boolean" },
                    titleHeader: { type: "boolean" },
                    provider: { type: "string" },
                    accessToken: { type: "string" },
                    status: { type: "string" }
                }
            }
        }
    })


    const memory = new Memory({ schema, name: "memory" });

    const database = new Database({ name: "database", schema });

    const coordinator = new Coordinator({
        sources: [memory, database]
    });

    const databaseMemorySync = new SyncStrategy({
        source: "memory",
        target: "database",
        blocking: true
    })

    return {
        memory,
        database,
        coordinator,

        activate: async function () {
            const transform = await database.pull(query => query.findRecords());

            coordinator.addStrategy(databaseMemorySync);

            await memory.sync(transform);
            await coordinator.activate();
        }
    }
}


export default init();