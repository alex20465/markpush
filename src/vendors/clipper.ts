import { StrictMode } from "react";
import md5 from "md5";
import { Article, ArticleImage } from "../types";
import slugify from "slugify";
const { Readability } = require("@mozilla/readability");
const { default: TurndownService } = require('turndown')
const TurndownGFM = require('turndown-plugin-gfm')


export async function fetchArticle(url: string, enhanceRemoveImagesToLocal: boolean): Promise<Article> {

    const html = await fetchHTML(url);

    const dom = parseHTML(html);

    replaceIncorrectDocumentSources(dom, url);

    const article = parseDOM(dom);

    let {content, images} = parseArticleHTML(article.content, enhanceRemoveImagesToLocal);

    return {
        title: article.title,
        source: url,
        content,
        images
    }
}

async function fetchHTML(url: string): Promise<string> {
    const response = await fetch(url);
    return response.text();
}

function parseHTML(html: string): Document {
    const parser = new DOMParser();

    return parser.parseFromString(html, "text/html");
}

function replaceIncorrectDocumentSources(dom: Document, siteUrl: string): Document {
    const baseURL = new URL(siteUrl);

    dom.querySelectorAll("img[src]")
        .forEach(element => forceToAbsolute(element, baseURL, "src"));

    dom.querySelectorAll("a[href]")
        .forEach(element => forceToAbsolute(element, baseURL, "href"));

    return dom;
}

function forceToAbsolute(element: Element, baseURL: URL, tagName = "src"): Element {
    try {
        const url = new URL(element.getAttribute(tagName), baseURL);
        element.setAttribute(tagName, url.href);
    } catch (err) {
        /**
         * Source was invalid.
         */
    }
    return element;
}


function parseDOM(dom: Document): Article {
    const reader = new Readability(dom);
    return reader.parse();
}

function cleanAttribute (attribute) {
    return attribute ? attribute.replace(/(\n+\s*)+/g, '\n') : ''
}

function parseArticleHTML(html: string, enhanceRemoveImagesToLocal: boolean) {
    const service = new TurndownService();

    /**
     * @Todo: Bug - table.th[colspan=x] breaks the body ... fix my handling colspan values.
     */
    service.use(TurndownGFM.gfm)

    const images: ArticleImage[] = [];

    if(enhanceRemoveImagesToLocal)
        service.addRule("imageMarker", {
            filter: 'img',
            replacement: function (content, node) {
                const alt = cleanAttribute(node.getAttribute('alt'))
                const src = node.getAttribute('src') || ''

                if(!src) return "";

                let imageId: string = md5(src)

                if(alt) {
                    imageId = `${imageId}-${slugify(alt)}`
                }
                const url = new URL(src);

                const dest = `images/${url.hostname}`;

                images.push({
                    dest: dest,
                    id: imageId,
                    sourceUrl: src
                });

                const title = cleanAttribute(node.getAttribute('title'))
                const titlePart = title ? ` "${title}"` : ''

                return  `![${alt}](./${dest}/${imageId}${titlePart})`
            }
        })

    return {
        content: service.turndown(html),
        images
    };
}
