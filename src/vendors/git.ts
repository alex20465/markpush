import FS from '@isomorphic-git/lightning-fs';
import git from 'isomorphic-git';
import http from 'isomorphic-git/http/web';
import { Repository, RepositoryProvider } from '../types';

export class BaseGitError extends Error {
    constructor(message?: string) {
        const trueProto = new.target.prototype;
        super(message);
        Object.setPrototypeOf(this, trueProto);
    }
}

export class GitCloneError extends BaseGitError { }
export class GitPushError extends BaseGitError { }
export class GitAddError extends BaseGitError { }
export class GitCommitError extends BaseGitError { }


export class Git {

    private fs: FS;

    constructor(
        private dir: string,
        private url: string,
        private accessToken: string,
        private provider: RepositoryProvider) {

        this.fs = new FS("git");
    }

    async init() {
        this.fs.init({ force: true });
    }

    async size(): Promise<number> {
        return new Promise(resolve => {
            this.fs.du(this.dir, (err, size) => {
                resolve(size);
            });
        });
    }

    async add(filepath: string) {
        return git.add({
            fs: this.fs,
            dir: this.dir,
            filepath: this._splitDirs(filepath).join("/")
        }).catch(err => {
            throw new GitCommitError(err.message);
        });
    }

    async commit(message: string) {
        return git.commit({
            fs: this.fs,
            dir: this.dir,
            author: {
                /**
                 * @Todo: Change this
                 */
                name: "Alexandros",
                email: "fotiadis@alexandros.blue"
            },
            message
        }).catch(err => {
            throw new GitCommitError(err.message);
        });
    }

    async clone() {
        return git.clone({
            fs: this.fs,
            http,
            dir: this.dir,
            url: this.url,
            depth: 1,
            onAuth: this._onAuth
        }).catch(err => {
            throw new GitCloneError(err.message);
        });
    }

    async push() {
        return git.push({
            fs: this.fs,
            http,
            dir: this.dir,
            remote: "origin",
            url: this.url,
            onAuth: this._onAuth
        }).catch(err => {
            throw new GitPushError(err.message);
        })
    }

    /**
     * @source https://isomorphic-git.org/docs/en/authentication
     */
    private _onAuth = () => {
        switch (this.provider) {
            case RepositoryProvider.GITEA:
            case RepositoryProvider.GITLAB:
                return {
                    username: 'oauth2',
                    password: this.accessToken
                }
            case RepositoryProvider.GITHUB:
                return {
                    username: this.accessToken,
                    password: 'x-oauth-basic'
                }
            case RepositoryProvider.BITBUCKET:
                return {
                    username: 'x-token-auth',
                    password: this.accessToken
                }
        }
    }

    async write(filepath: string, data: string | Uint8Array | ArrayBuffer) {
        return new Promise((resolve, reject) => {
            this.fs.writeFile(this.join(this.dir, filepath), data, {}, (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(null);
                }
            });
        });
    }

    async mkdirP(path: string) {
        const dirs = this._splitDirs(path);

        const pathsToCheck = this._makePaths(dirs);

        for (const path of pathsToCheck) {
            const isDir = await this.isDir(path);
            if (isDir === false) {
                await this.mkdir(path);
            }
        }
    }

    join(...paths: string[]) {
        const dirs = paths.reduce((dirs, path) => {
            return dirs.concat(this._splitDirs(path));
        }, []);
        return `/${dirs.join("/")}`;
    }

    private _splitDirs(path: string) {
        return path.split("/").filter(dir => (dir !== ""));
    }

    private _makePath(dirs: string[]) {
        return `/${dirs.join("/")}`;
    }

    private _makePaths(dirs: string[]) {
        let currentPath = "";
        return dirs.map((dir) => {
            currentPath = [currentPath, dir].join("/");
            return currentPath;
        });
    }

    async isDir(path: string): Promise<boolean> {
        try {
            const response = await this.fs.promises.stat(this.join(this.dir, path));
            return response.isDirectory();
        } catch (err) {
            return false;
        }
    }

    async exists(path: string): Promise<boolean> {
        return this.fs.promises.stat(this.join(this.dir, path))
            .then(() => true)
            .catch(() => false);
    }

    async mkdir(path: string) {
        return new Promise((resolve, reject) => {
            this.fs.writeFile(this.join(this.dir, path), {}, (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    }

    async clear() {
        await this.rmdirR();
    }

    async unlink(path: string) {
        return this.fs.promises.unlink(this.join(this.dir, path));
    }

    async rmdirR(path: string = "") {
        const namespacedPath = this.join(this.dir, path);

        const isDirectory = await this.isDir(path);
        const exists = await this.exists(path);

        if (exists && isDirectory === false) {
            return this.unlink(namespacedPath);
        } else if (exists === false) {
            return; // not existing
        }

        const dirs = await this.fs.promises.readdir(namespacedPath);

        for (const dir of dirs) {
            const isDir = await this.isDir(dir);
            if (isDir) {
                await this.rmdirR(this.join(path, dir));
            } else {
                await this.unlink(this.join(path, dir));
            }
        }
    }

    async list(path: string) {
        return new Promise((resolve, reject) => {
            this.fs.readdir(this.join(this.dir, path), {}, (err, dirs) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(dirs)
                }
            });
        });
    }
}

export function factory(repository: Repository) {
    const { id, attributes: { accessToken, url, provider } } = repository;
    return new Git(`/${id}`, url, accessToken, provider);
}
