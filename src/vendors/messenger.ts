import { Article, Repository, RepositoryAttributes } from "../types";

export enum REQUEST_TYPE {
    ARTICLE = 'ARTICLE',
    ARTICLE_SAVE = 'ARTICLE_SAVE',
    ARTICLE_IMAGES_SAVE = 'ARTICLE_IMAGES_SAVE',
    REPOSITORY = 'REPOSITORY',
    REPOSITORY_SAVE = 'REPOSITORY_SAVE',
    REPOSITORY_DELETE = 'REPOSITORY_DELETE',
    REPOSITORY_INIT = 'REPOSITORY_INIT',
    REPOSITORY_UPDATE = 'REPOSITORY_UPDATE',
    REPOSITORIES = 'REPOSITORIES',
}

export enum RESPONSE_TYPE {
    ARTICLE = 'ARTICLE',
    ARTICLE_SAVE = 'ARTICLE_SAVE',
    ARTICLE_IMAGES_SAVE = 'ARTICLE_IMAGES_SAVE',
    REPOSITORY = 'REPOSITORY',
    REPOSITORY_SAVE = 'REPOSITORY_SAVE',
    REPOSITORY_DELETE = 'REPOSITORY_DELETE',
    REPOSITORY_INIT = 'REPOSITORY_INIT',
    REPOSITORY_UPDATE = 'REPOSITORY_UPDATE',
    REPOSITORIES = 'REPOSITORIES',
    ERROR = 'ERROR'
}

export abstract class AbstractRequest<B = any> {
    abstract id: REQUEST_TYPE;

    constructor(public readonly body: B) { }
}

export abstract class AbstractResponse<B = any> {
    abstract id: RESPONSE_TYPE;

    constructor(public readonly body: B) { }
}


/**
 * REQUESTS
 */
export class RequestArticle extends AbstractRequest<{ url: string, downloadImages: boolean }> {
    id = REQUEST_TYPE.ARTICLE;
}

export class RequestArticleImages extends AbstractRequest<{ article: Article, repositoryId: string }> {
    id = REQUEST_TYPE.ARTICLE_IMAGES_SAVE;
}

export class RequestArticleSave extends AbstractRequest<{ article: Article, repositoryId: string }> {
    id = REQUEST_TYPE.ARTICLE_SAVE;
}

export class RequestRepository extends AbstractRequest<{ id: string }> {
    id = REQUEST_TYPE.REPOSITORY;
}

export class RequestRepositorySave extends AbstractRequest<RepositoryAttributes> {
    id = REQUEST_TYPE.REPOSITORY_SAVE;
}

export class RequestRepositoryDelete extends AbstractRequest<{ id: string }> {
    id = REQUEST_TYPE.REPOSITORY_DELETE;
}

export class RequestRepositoryUpdate extends AbstractRequest<Repository> {
    id = REQUEST_TYPE.REPOSITORY_UPDATE;
}

export class RequestRepositories extends AbstractRequest<void> {
    id = REQUEST_TYPE.REPOSITORIES;
}

export class RequestRepositoryInit extends AbstractRequest<{ id: string }> {
    id = REQUEST_TYPE.REPOSITORY_INIT;
}

/**
 * RESPONSES
 */

 export class ResponseArticle extends AbstractResponse<Article | null> {
    id = RESPONSE_TYPE.ARTICLE;
}

export class ResponseArticleImages extends AbstractResponse<Article | null> {
    id = RESPONSE_TYPE.ARTICLE_IMAGES_SAVE;
}

export class ResponseArticleSave extends AbstractResponse<boolean> {
    id = RESPONSE_TYPE.ARTICLE_SAVE;
}

export class ResponseRepository extends AbstractResponse<Repository> {
    id = RESPONSE_TYPE.REPOSITORY;
}

export class ResponseRepositorySave extends AbstractResponse<Repository> {
    id = RESPONSE_TYPE.REPOSITORY_SAVE;
}
export class ResponseRepositoryDelete extends AbstractResponse<boolean> {
    id = RESPONSE_TYPE.REPOSITORY_DELETE;
}

export class ResponseRepositoryUpdate extends AbstractResponse<Repository> {
    id = RESPONSE_TYPE.REPOSITORY_UPDATE;
}

export class ResponseRepositories extends AbstractResponse<Repository[]> {
    id = RESPONSE_TYPE.REPOSITORIES;
}

export class ResponseRepositoryInit extends AbstractResponse<boolean> {
    id = RESPONSE_TYPE.REPOSITORY_INIT;
}


export class ResponseError extends AbstractResponse<{ name: string, message: string }> {
    id = RESPONSE_TYPE.ERROR;
}

const REQUEST_REGISTRY = [
    {
        id: REQUEST_TYPE.ARTICLE,
        class: RequestArticle
    },
    {
        id: REQUEST_TYPE.ARTICLE_SAVE,
        class: RequestArticleSave
    },
    {
        id: REQUEST_TYPE.ARTICLE_IMAGES_SAVE,
        class: RequestArticleImages,
    },
    {
        id: REQUEST_TYPE.REPOSITORIES,
        class: RequestRepositories
    },
    {
        id: REQUEST_TYPE.REPOSITORY,
        class: RequestRepository
    },
    {
        id: REQUEST_TYPE.REPOSITORY_SAVE,
        class: RequestRepositorySave
    },
    {
        id: REQUEST_TYPE.REPOSITORY_UPDATE,
        class: RequestRepositoryUpdate
    },
    {
        id: REQUEST_TYPE.REPOSITORY_INIT,
        class: RequestRepositoryInit
    },
    {
        id: REQUEST_TYPE.REPOSITORY_DELETE,
        class: RequestRepositoryDelete
    }
];

const RESPONSE_REGISTRY = [
    {
        id: RESPONSE_TYPE.ARTICLE,
        class: ResponseArticle
    },
    {
        id: RESPONSE_TYPE.ARTICLE_SAVE,
        class: ResponseArticleSave
    },
    {
        id: RESPONSE_TYPE.ARTICLE_IMAGES_SAVE,
        class: ResponseArticleImages,
    },
    {
        id: RESPONSE_TYPE.ERROR,
        class: ResponseError
    },
    {
        id: RESPONSE_TYPE.REPOSITORIES,
        class: ResponseRepositories
    },
    {
        id: RESPONSE_TYPE.REPOSITORY,
        class: ResponseRepository
    },
    {
        id: RESPONSE_TYPE.REPOSITORY_SAVE,
        class: ResponseRepositorySave
    },
    {
        id: RESPONSE_TYPE.REPOSITORY_UPDATE,
        class: ResponseRepositoryUpdate
    },
    {
        id: RESPONSE_TYPE.REPOSITORY_INIT,
        class: ResponseRepositoryInit
    },
    {
        id: RESPONSE_TYPE.REPOSITORY_DELETE,
        class: ResponseRepositoryDelete
    }
]



export type MessageHandler = (message: AbstractRequest) => Promise<AbstractResponse>;


abstract class AbstractMessenger {

    abstract sendMessage(message: AbstractRequest): Promise<any>;

    abstract onMessage(handler: MessageHandler);

    protected load(messageObject: any): AbstractRequest | AbstractResponse | any {
        const { id = null, body = {}, type = null } = messageObject;
        if (type === "request") {
            const [record] = REQUEST_REGISTRY.filter(record => record.id === id);
            return new record.class(body);
        } else if (type === "response") {
            const [record] = RESPONSE_REGISTRY.filter(record => record.id === id);
            return new record.class(body);
        }
    }

    protected dump(instance: AbstractRequest | AbstractResponse): any {
        return {
            type: instance instanceof AbstractRequest ? "request" : "response",
            id: instance.id,
            body: instance.body
        }
    }
}

export class ChromeMessenger extends AbstractMessenger {

    async sendMessage(message: AbstractRequest) {
        return new Promise((resolve) => {
            chrome.runtime.sendMessage(this.dump(message), (response) => {
                resolve(this.load(response));
            });
        });
    }

    onMessage(handler: MessageHandler) {
        chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
            const message = this.load(request);

            if (message instanceof AbstractRequest) {
                handler(message)
                    .then((response) => {
                        sendResponse(this.dump(response));
                    });
            }
            return true;
        });
    }
}

export class BrowserMessenger extends AbstractMessenger {
    async sendMessage(message: AbstractRequest) {
        return new Promise((resolve) => {
            browser.runtime.sendMessage(this.dump(message), (response) => {
                resolve(this.load(response));
            });
        });
    }

    onMessage(handler: MessageHandler) {
        browser.runtime.onMessage.addListener((request, sender, sendResponse) => {
            const message = this.load(request);

            if (message instanceof AbstractRequest) {
                handler(message)
                    .then((response) => {
                        sendResponse(this.dump(response));
                    });
            }
            return true;
        });
    }
}


export function factory(): AbstractMessenger {

    if (window.browser) {
        return new BrowserMessenger();
    } else if (window.chrome) {
        // chrome is not a browser, chrome is chrome. :D
        return new ChromeMessenger();
    } else {
        throw new Error("Runtime not supported!");
    }
}

export const messenger = factory();
