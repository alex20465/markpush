import * as React from 'react';

import "./style.less"
import { Link, RouteComponentProps, withRouter } from 'react-router-dom';
import { Layout, Menu, PageHeader, Spin } from 'antd';
import { PageHeaderProps } from 'antd/lib/page-header';

import { compose } from "recompose";
import { BranchesOutlined, FileMarkdownFilled, GitlabFilled } from '@ant-design/icons';

const { Header, Content } = Layout;

interface OuterProps {
    loading?: boolean
    loadingMessage?: string
    pageHeader: PageHeaderProps
}

interface State {
}

type Props = RouteComponentProps & OuterProps;

class BaseLayout extends React.Component<Props, State> {

    private getSelectedMenuKeys = (): string[] => {
        const { location: { pathname } } = this.props;
        let currentDepth = ""
        const paths = pathname.split("/").filter(part => part !== "").map(part => {
            currentDepth = [currentDepth, part].join("/");
            return `${currentDepth}`;
        });

        return paths.length ? paths : ["/"] // default root;
    }


    render() {
        const { children, loading, pageHeader, loadingMessage = "" } = this.props;
        return (
            <Layout className="base-layout">
                <Header>
                    <div className="logo">
                        <img src="/assets/icon128.png" />
                    </div>
                    <Menu theme="dark" mode="horizontal" selectedKeys={this.getSelectedMenuKeys()}>
                        <Menu.Item icon={<FileMarkdownFilled />} key="/">
                            Clipper
                            <Link to={"/"} />
                        </Menu.Item>
                        <Menu.Item icon={<BranchesOutlined />} key="/repository">
                            Repositories
                            <Link to={"/repository"} />
                        </Menu.Item>
                    </Menu>
                </Header>

                <PageHeader  {...pageHeader} className={loading ? "page-header-loading" : ""} />
                <Content>
                    <div className="site-layout-content">
                        <Spin spinning={loading || false} tip={`${loadingMessage} ...`}>
                            {children}
                        </Spin>
                    </div>
                </Content>
                <Layout.Footer>
                    Mark <strong>PUSH</strong> by <a href="mailto:fotiadis@alexandros.blue">alexandros.blue</a> | <a href="https://gitlab.com/alex20465/markpush"><GitlabFilled /> Gitlab</a>
                </Layout.Footer>
            </Layout>
        );
    }

}

export default compose<Props, OuterProps>(
    withRouter
)(BaseLayout);