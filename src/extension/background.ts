import database from "../vendors/database";
import * as repository from "./handlers/repository";
import * as article from "./handlers/article";

import * as m from "../vendors/messenger";

import { Buffer } from 'buffer'

(window as any).Buffer = Buffer;
(window as any).process = {
    cwd: () => { }
};


const messenger = m.factory();

const HANDLER_REGISTRY = [...repository.HANDLER_REGISTRY, ...article.HANDLER_REGISTRY];

async function main() {

    await database.activate();

    messenger.onMessage(async (message) => {

        console.debug(`Handle Message [${message.id}]`, message);

        const MATCHES = HANDLER_REGISTRY.filter(item => message instanceof item.class);
        if (!MATCHES.length) {
            return new m.ResponseError({
                name: "UnhandledMessage",
                message: `message ${message.id} has no registered handler`
            });
        } else {
            const [{ handle, errorHandler = null }] = MATCHES;

            try {
                const response: m.AbstractResponse = await handle(message);

                console.debug(`Handle Response [${message.id} => ${response.id}]`, {
                    request: message,
                    response
                });

                return response;
            } catch (err) {
                console.error(`Handler [${message.id}] ERROR: `, err);

                if (errorHandler) {
                    return await errorHandler(err, message);
                } else {
                    return new m.ResponseError({
                        name: "HandlerError",
                        message: err.message
                    });
                }
            }
        }
    });
}

main()
    .then(() => {
        console.log("background script loaded successfully.");
    })
    .catch(error => {
        console.error(error);
    });