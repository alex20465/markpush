
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { HashRouter as Router, Redirect, Route, Switch } from "react-router-dom";
import ClipperView from "../views/popup/clipper";

import "./style.less";

import database from '../vendors/database';
import RepositoryListView from '../views/popup/repository/List';
import RepositoryCreateView from '../views/popup/repository/Create';
import RepositoryEditView from '../views/popup/repository/Edit';


interface Props {
    source: string;
}

interface State {
    loading: boolean,
}

class PopUp extends React.Component<Props, State> {

    state = {
        loading: true,
    } as State;

    componentDidMount = async () => {
        this.setState({ loading: true });
        await database.activate();
        this.setState({ loading: false });
    }

    render() {

        if (this.state.loading) {
            return null;
        }

        return (
            <Router>
                <Route path="/" exact component={(props) => <ClipperView {...props} source={this.props.source} />} />
                <Route path="/repository" exact component={RepositoryListView} />
                <Route path="/repository/create" exact component={RepositoryCreateView} />
                <Route path="/repository/edit/:id" exact component={RepositoryEditView} />
            </Router>
        );
    }
}


/**
 * FireFox provides the same API, but lets separate just in-case they drop support OR additional specific
 * APIs are used in the future.
 */

if (window.browser) {
    browser.tabs.query({ active: true, currentWindow: true })
        .then((tabs) => {
            const [activeTab] = tabs;
            ReactDOM.render(<PopUp source={activeTab.url || ""} />, document.getElementById('popup-view'));
        });
} else if (window.chrome) {
    chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
        const [activeTab] = tabs;
        ReactDOM.render(<PopUp source={activeTab.url || ""} />, document.getElementById('popup-view'));
    });
}
