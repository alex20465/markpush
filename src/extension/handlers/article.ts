import { Article, ArticleImage, Repository } from "../../types";
import { fetchArticle } from "../../vendors/clipper";
import database from "../../vendors/database";
import { factory as gitFactory, Git } from "../../vendors/git";
import { RequestArticle, RequestArticleImages, RequestArticleSave, ResponseArticle, ResponseArticleImages, ResponseArticleSave, RESPONSE_TYPE } from "../../vendors/messenger";
import { HandlerRegistryRecord } from "./types";
import {Promise as BPromise} from "bluebird"


const ALLOWED_IMAGE_TYPES = ["image/png", "image/jpeg"];
const MAX_IMAGE_SIZE = 1000 * 1000 * 10; // 10 MB
const CONCURRENT_DOWNLOADS = 10;

export const HANDLER_REGISTRY: HandlerRegistryRecord[] = [
    {
        class: RequestArticle,
        handle: handleRequestArticle
    },
    {
        class: RequestArticleSave,
        handle: handleRequestArticleSave
    },
    {
        class: RequestArticleImages,
        handle: handleRequestArticleImages
    }
]

async function handleRequestArticleImages(message: RequestArticleImages): Promise<ResponseArticleImages> {
    const { article, repositoryId } = message.body;

    const repo: Repository = await database.memory.query(operator => operator.findRecord({
        id: repositoryId,
        type: "repository"
    }));

    const git = gitFactory(repo);

    await git.init()

    const itemsToAddOnGit = await BPromise.map(article.images, async (image) => {
        const data = await fetchImage(image);
        if(!data) return null;
        else return {image, data};
    }, {concurrency: CONCURRENT_DOWNLOADS}).filter(res => !!res);


    for (const item of itemsToAddOnGit){
        const filepath = await writeImage(git, repo, item.image, item.data);
        await git.add(filepath);
    }

    await git.commit(`ADD: Images of ${article.title}`);

    return new ResponseArticle(article);
}

async function fetchImage(image: ArticleImage): Promise<ArrayBuffer> {
    const res = await fetch(image.sourceUrl);

    // accept only 2xx
    if(res.status < 200 || res.status >= 300) return

    const type = res.headers.get("Content-Type");
    const length = res.headers.get("Content-Length");

    if (ALLOWED_IMAGE_TYPES.indexOf(type) === -1){
        return;
    } else if (parseInt(length) > MAX_IMAGE_SIZE) {
        return;
    }

    console.debug(`Downloading image`, {image});

    return res.arrayBuffer();
}

async function writeImage(git: Git, repo: Repository, image: ArticleImage, imageBytes: ArrayBuffer) {
    const dir = git.join(repo.attributes.location, image.dest);
    const filepath = git.join(repo.attributes.location, image.dest, image.id);

    await git.mkdirP(dir)
    await git.write(filepath, imageBytes);

    return filepath;
}

function includeTitleInMarkdownContent(article): Article {
    return {
        ...article,
        content: [`# ${article.title}`, "---", article.content].join(`\n`)
    }
}

async function handleRequestArticle(message: RequestArticle): Promise<ResponseArticle> {

    const article = await fetchArticle(message.body.url, message.body.downloadImages);

    return new ResponseArticle(article);
}

async function handleRequestArticleSave(message: RequestArticleSave): Promise<ResponseArticleSave> {

    const { article, repositoryId } = message.body;

    const record: Repository = await database.memory.query(operator => operator.findRecord({
        id: repositoryId,
        type: "repository"
    }));

    const repo = gitFactory(record);

    await repo.init();

    const filename = `${article.title}.md`;

    await repo.mkdirP(record.attributes.location)

    const filepath = repo.join(record.attributes.location, filename);

    let modifiedArticle = {...article};

    if(record.attributes.titleHeader) {
        modifiedArticle = includeTitleInMarkdownContent(article)
    }

    await repo.write(filepath, modifiedArticle.content);
    await repo.add(filepath);
    await repo.commit(`ADD: Article ${modifiedArticle.title} from ${modifiedArticle.source}`);
    await repo.push();

    return new ResponseArticleSave(true);
}