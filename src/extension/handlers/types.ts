import { AbstractRequest, AbstractResponse } from "../../vendors/messenger";


export interface HandlerRegistryRecord {
    handle: (message: AbstractRequest) => Promise<AbstractResponse>
    errorHandler?: (err: Error, message: AbstractRequest) => Promise<AbstractResponse>
    class: any
};