import { Repository, RepositoryStatus } from "../../types";
import database from "../../vendors/database";
import { factory as gitFactory, BaseGitError } from "../../vendors/git";

import { v4 } from "uuid";

import {
    RequestRepositories,
    RequestRepository,
    RequestRepositoryDelete,
    RequestRepositoryInit,
    RequestRepositorySave,
    RequestRepositoryUpdate,
    ResponseError,
    ResponseRepositories,
    ResponseRepository,
    ResponseRepositoryDelete,
    ResponseRepositoryInit,
    ResponseRepositorySave,
    ResponseRepositoryUpdate
} from "../../vendors/messenger";
import { HandlerRegistryRecord } from "./types";


export const HANDLER_REGISTRY: HandlerRegistryRecord[] = [
    {
        class: RequestRepositories,
        handle: list
    },
    {
        class: RequestRepositorySave,
        handle: save
    },
    {
        class: RequestRepositoryInit,
        handle: init,
        errorHandler: initErrorHandler
    },
    {
        class: RequestRepositoryUpdate,
        handle: update
    },
    {
        class: RequestRepository,
        handle: get
    },
    {
        class: RequestRepositoryDelete,
        handle: _delete
    }
]


export async function list(message: RequestRepositories): Promise<ResponseRepositories> {

    const repositories: Repository[] = await database.memory.query((query) => query.findRecords("repository"));

    return new ResponseRepositories(repositories);
}

export async function save(message: RequestRepositorySave): Promise<ResponseRepositorySave> {
    const id = v4();

    await database.memory.update(operator => operator.addRecord({
        type: "repository",
        id,
        attributes: message.body
    }));

    return new ResponseRepositorySave({
        id,
        attributes: {
            ...message.body,
            status: RepositoryStatus.DEFINED
        }
    });
}

export async function update(message: RequestRepositoryUpdate): Promise<ResponseRepositoryUpdate> {

    const { id, attributes } = message.body;

    await database.memory.update(operator => operator.updateRecord({
        type: "repository",
        id: message.body.id,
        attributes: message.body.attributes
    }));

    return new ResponseRepositoryUpdate({
        id,
        attributes: {
            ...attributes,
            status: RepositoryStatus.DEFINED
        }
    });
}

export async function get(message: RequestRepository): Promise<ResponseRepository> {

    const repository: Repository = await database.memory.query(operator => operator.findRecord({
        id: message.body.id,
        type: "repository"
    }));

    return new ResponseRepository(repository);
}


export async function init(message: RequestRepositoryInit): Promise<ResponseRepositoryInit> {

    const record: Repository = await database.memory.query(operator => operator.findRecord({
        id: message.body.id,
        type: "repository"
    }));

    const repo = gitFactory(record);

    await repo.init();

    console.debug("clear repo", { record, repo });

    await repo.clear();

    console.debug("clone repo", { record, repo });

    await repo.clone();

    console.log("repository successfully initialized");

    await database.memory.update(operator => operator.updateRecord({
        type: "repository",
        id: record.id,
        attributes: {
            ...record.attributes,
            status: RepositoryStatus.SYNC
        }
    }));

    return new ResponseRepositoryInit(true);
}

export async function initErrorHandler(err: Error, message: RequestRepositoryInit): Promise<ResponseError> {

    let status: RepositoryStatus;

    if (err instanceof BaseGitError) {
        status = RepositoryStatus.OUT_SYNC;
    } else {
        status = RepositoryStatus.ERROR;
    }

    const record: Repository = await database.memory.query(operator => operator.findRecord({
        id: message.body.id,
        type: "repository",
    }));

    await database.memory.update(operator => operator.updateRecord({
        id: record.id,
        type: "repository",
        attributes: {
            ...record.attributes,
            status: status
        }
    }));

    return new ResponseError({
        name: status === RepositoryStatus.OUT_SYNC ? "GitError" : "Error",
        message: err.message
    });
}

export async function _delete(message: RequestRepositoryDelete): Promise<ResponseRepositoryDelete> {
    const { id } = message.body;
    await database.memory.update(operator => operator.removeRecord({ id, type: "repository" }));
    return new ResponseRepositoryDelete(true);
}