import { Button, Form, Input, Select } from 'antd';
import React, { Component } from 'react'

interface Props {
    onFinish: (FormData) => void
}
interface State {
    storageLocation: string | null
}

export interface SettingsFormData {
    repository: string
    privateKey: string
}

export default class Settings extends Component<Props, State> {
    state = {
        storageLocation: null
    } as State;


    componentDidMount = () => {

    }

    onFinish = async (data: SettingsFormData) => {

    }

    render() {
        return (
            <Form onFinish={this.onFinish}>
                <Form.Item name="repository" label="Repository" extra="The git repository URL">
                    <Input />
                </Form.Item>

                <Form.Item name="provider" label="Provider" >
                    <Select defaultValue="gitea" options={[
                        {
                            value: "gitea",
                            label: "Gitea"
                        },
                    ]} />
                </Form.Item>

                <Form.Item name="accessToken" label="Access Token">
                    <Input />
                </Form.Item>

                <Form.Item>
                    <Button type="primary" block htmlType="submit">Save</Button>
                </Form.Item>
            </Form>
        )
    }
}
