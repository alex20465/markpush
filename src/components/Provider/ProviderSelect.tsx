import { GithubFilled, GitlabFilled } from '@ant-design/icons'
import { Select, Typography } from 'antd'
import { SelectProps } from 'antd/lib/select'
import React, { Component } from 'react'
import { RepositoryProvider } from '../../types'
import GiteaIcon from '../../icons/GiteaIcon';
import BitbucketIcon from '../../icons/BitbucketIcon';

interface State {

}

export default class ProviderSelect extends Component<SelectProps<RepositoryProvider>, State> {
    state = {}

    render() {
        return (
            <Select {...this.props} options={[
                {
                    value: RepositoryProvider.GITEA,
                    label: (
                        <Typography.Text>
                            <GiteaIcon /> Gitea
                        </Typography.Text>
                    )
                },
                {
                    value: RepositoryProvider.GITLAB,
                    label: (
                        <Typography.Text>
                            <GitlabFilled /> GitLab
                        </Typography.Text>
                    )
                },
                {
                    value: RepositoryProvider.GITHUB,
                    label: (
                        <Typography.Text>
                            <GithubFilled /> GitHub
                        </Typography.Text>
                    )
                },
                {
                    value: RepositoryProvider.BITBUCKET,
                    label: (
                        <Typography.Text>
                            <BitbucketIcon /> Bitbucket
                        </Typography.Text>
                    )
                }
            ]} />
        )
    }
}
