import { Button, Input } from 'antd'
import Form, { FormInstance, FormProps } from 'antd/lib/form'
import React, { Component } from 'react'
import RepositorySelect from '../repository/RepositorySelect'

interface State {

}

interface Props extends FormProps<ArticleDestinationFormData> {
    renderSubmit?: boolean
    formRef?: React.RefObject<FormInstance>
}

export interface ArticleDestinationFormData {
    repository: string
}

export default class ArticleDestinationForm extends Component<Props, State> {
    state = {}

    render() {

        const { renderSubmit = true, formRef = undefined, ...props } = this.props;
    
        return (
            <Form {...props} ref={formRef} >
                <Form.Item
                    rules={[
                        {
                            required: true
                        }
                    ]}
                    name="repository" label="Repository">
                    <RepositorySelect />
                </Form.Item>
                <Form.Item hidden={!renderSubmit}>
                    <Button htmlType="submit">Submit</Button>
                </Form.Item>
            </Form>
        )
    }
}
