import { InfoCircleOutlined } from '@ant-design/icons'
import { Button, Input, Skeleton, Tooltip } from 'antd'
import Form, { FormInstance, FormProps } from 'antd/lib/form'
import React, { Component } from 'react'

interface State {

}

interface Props extends FormProps<ArticleFormData> {
    renderSubmit?: boolean
    formRef?: React.RefObject<FormInstance>
    loading?: boolean
}

export interface ArticleFormData {
    title: string
    source: string
}

export default class ArticleForm extends Component<Props, State> {
    state = {}

    renderSkeleton = () => {
        return (
            <Skeleton active={true} paragraph={{
                rows: 4,
                width: "100%"
            }} />
        )
    }

    render() {

        const { loading, renderSubmit = true, formRef = undefined, ...props } = this.props;

        if (loading) {
            return this.renderSkeleton();
        }

        return (
            <Form {...props} ref={formRef} layout="vertical">
                <Form.Item
                    rules={[
                        {
                            required: true
                        }
                    ]}
                    name="title" label="Title">
                    <Input
                        suffix={
                            <Tooltip placement="left" title="The article title used as a <filename>.md">
                                <InfoCircleOutlined />
                            </Tooltip>
                        }
                        placeholder="UltraHighHyperPro... bro" />
                </Form.Item>

                <Form.Item
                    rules={[
                        {
                            required: true
                        },
                        {
                            type: "url"
                        }
                    ]}
                    name="source" label="Source">
                    <Input />
                </Form.Item>

                <Form.Item hidden={!renderSubmit}>
                    <Button htmlType="submit">Submit</Button>
                </Form.Item>
            </Form>
        )
    }
}
