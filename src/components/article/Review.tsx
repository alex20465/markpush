import React, { Component } from 'react'
import ReactMarkdown from 'react-markdown'
import { Article } from '../../types';
import ArticleForm from './ArticleForm';

interface Props {
    article: Article
}
interface State {
    
}

export default class Review extends Component<Props, State> {
    state = {}

    render() {

        const { article } = this.props;

        return (
            <React.Fragment>
                <ArticleForm initialValues={{
                    title: article.title
                }} />
                <ReactMarkdown>{article.content}</ReactMarkdown>
            </React.Fragment>
        )
    }
}
