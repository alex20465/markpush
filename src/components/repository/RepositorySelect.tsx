import Select, { SelectProps } from 'antd/lib/select'
import React, { Component } from 'react'
import { Repository } from '../../types';
import { messenger, RequestRepositories, ResponseRepositories } from '../../vendors/messenger';

interface Props {

}
interface State {
    items: Repository[]
}

export default class RepositorySelect extends Component<SelectProps<string>, State> {
    state = {
        items: []
    } as State;

    componentDidMount = async () => {
        const response: ResponseRepositories = await messenger.sendMessage(new RequestRepositories());
        this.setState({ items: response.body });
    }

    getOptions = () => {
        return this.state.items.map(item => {
            return {
                value: item.id,
                label: item.attributes.name
            } as any;
        })

    }

    render() {
        return (
            <Select {...this.props} options={this.getOptions()} />
        )
    }
}
