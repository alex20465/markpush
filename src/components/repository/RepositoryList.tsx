import { Button, List, Typography } from 'antd';
import ButtonGroup from 'antd/lib/button/button-group';
import React, { Component } from 'react';
import { Repository, RepositoryProvider, RepositoryStatus } from '../../types';
import { messenger, RequestRepositories } from '../../vendors/messenger';

import EditOutlined from "@ant-design/icons/EditOutlined";

import GiteaIcon from "../../icons/GiteaIcon";
import BitbucketIcon from "../../icons/BitbucketIcon";
import Avatar from 'antd/lib/avatar/avatar';
import { DownloadOutlined } from '../../ui/icons';
import { GithubFilled, GitlabFilled } from '@ant-design/icons';

interface Props {
    onClickClone: (item: Repository) => void
    onClickEdit: (item: Repository) => void
}
interface State {
    items: Repository[]
}

export default class RepositoryList extends Component<Props, State> {
    state = {
        items: []
    } as State;


    componentDidMount = async () => {
        const response = await messenger.sendMessage(new RequestRepositories());
        this.setState({ items: response.body });
    }

    renderItemDescription = (item: Repository) => {
        const { attributes: { status } } = item;
        if (status === RepositoryStatus.OUT_SYNC) {
            return (
                <Typography.Text type="warning">
                    Out of sync, current state can't be resolved
                </Typography.Text>
            )
        } else if (status === RepositoryStatus.ERROR) {
            return (
                <Typography.Text type="danger">
                    Unexpected error
                </Typography.Text>
            )
        } else if (status === RepositoryStatus.DEFINED) {
            return (
                <Typography.Text type="warning">
                    Checkout required
                </Typography.Text>
            )
        } else if (status === RepositoryStatus.SYNC) {
            return (
                <Typography.Text type="success">
                    Synchronized
                </Typography.Text>
            )
        }
    }

    getProviderIcon(provider: RepositoryProvider) {
        switch (provider) {
            case (RepositoryProvider.GITEA):
                return (<GiteaIcon />)
            case (RepositoryProvider.GITLAB):
                return (<GitlabFilled />)
            case (RepositoryProvider.GITHUB):
                return (<GithubFilled />)
            case (RepositoryProvider.BITBUCKET):
                return (<BitbucketIcon />)
        }
    }

    renderItem = (item: Repository) => {
        const { onClickClone, onClickEdit } = this.props;

        return (
            <List.Item
                extra={(
                    <ButtonGroup>
                        <Button title="Edit" type="ghost" icon={<EditOutlined />} onClick={() => onClickEdit(item)} />
                        <Button
                            title="Clone"
                            type="ghost"
                            icon={<DownloadOutlined />}
                            onClick={() => onClickClone(item)} />
                    </ButtonGroup>
                )}>
                <List.Item.Meta
                    avatar={<Avatar shape="square" size="large" style={{ backgroundColor: "#5d9425" }} icon={this.getProviderIcon(item.attributes.provider)} />}
                    description={this.renderItemDescription(item)}
                    title={item.attributes.name} />
            </List.Item>
        );
    }

    render() {
        const { items } = this.state;

        return (
            <List dataSource={items} renderItem={this.renderItem} />
        )
    }
}
