import { InfoCircleOutlined } from '@ant-design/icons'
import { Button, Checkbox, Divider, Form, Input, Switch, Tooltip } from 'antd'
import { FormInstance, FormProps } from 'antd/lib/form'
import React, { Component } from 'react'
import { RepositoryProvider } from '../../types'
import ProviderSelect from '../Provider/ProviderSelect'

interface State {
}

export interface RepositoryFormData {
    name: string
    url: string
    location: string
    provider: RepositoryProvider
    accessToken: string
    downloadImages: boolean
    titleHeader: boolean
    clone: boolean
}

interface Props extends FormProps<RepositoryFormData> {
    showSubmit?: boolean
    formRef?: React.RefObject<FormInstance<RepositoryFormData>>
}

export default class RepositoryCreate extends Component<Props, State> {
    state = {}

    render() {

        const { showSubmit = true, formRef, ...props } = this.props;

        return (
            <Form {...props} ref={formRef} layout="vertical">
                <Form.Item
                    rules={[
                        {
                            required: true,
                        }
                    ]}
                    label="Alias" name="name">
                    <Input
                        suffix={
                            <Tooltip placement="left" title="Internal repository name">
                                <InfoCircleOutlined />
                            </Tooltip>
                        }
                        placeholder="my-personal-articles" />
                </Form.Item>
                <Form.Item
                    rules={[
                        {
                            required: true,
                        },
                        {
                            type: "url",
                        }
                    ]}
                    label="Remote URL" name="url">
                    <Input
                        suffix={
                            <Tooltip placement="left" title="HTTPS based GIT repository URL">
                                <InfoCircleOutlined />
                            </Tooltip>
                        }
                        placeholder="https://github.com/maximan/xxx" />
                </Form.Item>
                <Form.Item
                    rules={[
                        {
                            required: true,
                        },
                        {
                            pattern: /^\/.*$/,
                            message: "'location' is not a valid path"
                        }
                    ]}
                    label="Location" name="location">
                    <Input
                        suffix={
                            <Tooltip placement="left" title="The repository location in which the articles should be committed">
                                <InfoCircleOutlined />
                            </Tooltip>
                        }
                        placeholder="/location/to/store/articles ..." />
                </Form.Item>

                <Form.Item
                    help="GIT version control provider in which the repository is hosted"
                    rules={[
                        {
                            required: true
                        }
                    ]}
                    label="Provider" name="provider">
                    <ProviderSelect />
                </Form.Item>

                <Form.Item
                    rules={[
                        {
                            required: true
                        }
                    ]}
                    label="Access Token" name="accessToken">
                    <Input
                        type="password"
                        suffix={
                            <Tooltip placement="left" title="Generated application token for this application.">
                                <InfoCircleOutlined />
                            </Tooltip>
                        } />
                </Form.Item>

                <Divider type="horizontal">Markdown Features</Divider>
                <Form.Item
                    valuePropName="checked"
                    help="Download all article associated images and push into the repository under 'images/<image-host>/checksum'"
                    name="downloadImages">
                    <Checkbox>
                        Download Images
                    </Checkbox>
                </Form.Item>
                <Form.Item
                    valuePropName="checked"
                    help="Include the article title as markdown top-header title."
                    name="titleHeader">
                    <Checkbox defaultChecked >
                        Include Title
                    </Checkbox>
                </Form.Item>
                <Divider type="horizontal">On Save</Divider>
                <Form.Item help="Force fetch/pull the repository on save." name="clone" valuePropName="checked">
                    <Checkbox defaultChecked>
                        Fetch
                    </Checkbox>
                </Form.Item>

                <Form.Item hidden={!showSubmit} >
                    <Button htmlType="submit" block type="primary">Save</Button>
                </Form.Item>
            </Form>
        )
    }
}
