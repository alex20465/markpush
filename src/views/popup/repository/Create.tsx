import { SaveOutlined } from '@ant-design/icons';
import { Button, notification, Typography } from 'antd';
import { FormInstance } from 'antd/lib/form';
import React, { Component } from 'react'
import { RouteComponentProps } from 'react-router-dom'
import RepositoryForm, { RepositoryFormData } from '../../../components/repository/RepositoryForm'
import BaseLayout from '../../../layouts/BaseLayout'
import { RepositoryProvider } from '../../../types';
import { messenger, RequestRepositoryInit, RequestRepositorySave, ResponseRepositorySave } from '../../../vendors/messenger';
import { requestRepositoryInitResponseHandler } from './helpers';

type Props = RouteComponentProps;

interface State {
    loading: boolean
    loadingMessage: string
    formBackup: Partial<RepositoryFormData>
    restored: boolean
}

export default class RepositoryCreateView extends Component<Props, State> {
    state = {
        loading: false,
        loadingMessage: "",
        formBackup: {},
        restored: false
    } as State;

    form = React.createRef<FormInstance<RepositoryFormData>>();

    onFinish = async (data: RepositoryFormData) => {

        localStorage.removeItem("create-form-backup");

        this.setState({ loading: true, loadingMessage: "Saving" });
        const response = await this.save(data);

        notification.success({
            message: "Saved",
            description: (
                <Typography.Text>
                    Repository <strong>{data.name}</strong> has been saved.
                </Typography.Text>
            ),
            placement: "bottomLeft"
        });

        if (data.clone) {
            this.setState({ loading: true, loadingMessage: "Cloning" });
            await this.clone(response.body.id);
        }
        this.setState({ loading: false, loadingMessage: "" });
        this.props.history.push("/repository");
    }

    onChange = (data: RepositoryFormData) => {
        localStorage.setItem("create-form-backup", JSON.stringify(data));
    }

    componentDidMount = () => {
        const backup = localStorage.getItem("create-form-backup");
        if (backup) {
            this.setState({
                formBackup: JSON.parse(backup),
                restored: true
            })
        }
    }

    clone = async (id: string) => {
        const response = await messenger.sendMessage(new RequestRepositoryInit({ id }));
        requestRepositoryInitResponseHandler(response);
    }

    save = async (data: RepositoryFormData): Promise<ResponseRepositorySave> => {
        return messenger.sendMessage(new RequestRepositorySave(data));
    }

    render() {
        const { loading, loadingMessage } = this.state;

        return (
            <BaseLayout
                loading={loading}
                loadingMessage={loadingMessage}
                pageHeader={{
                    title: "Repository",
                    subTitle: "add",
                    onBack: () => this.props.history.push("/repository"),
                    extra: (
                        <Button
                            type="primary"
                            icon={<SaveOutlined />}
                            onClick={() => this.form.current.submit()}>
                            Save
                        </Button>
                    )
                }}>
                <RepositoryForm
                    key={this.state.restored ? "restored" : "initial"}
                    showSubmit={false}
                    onValuesChange={this.onChange}
                    formRef={this.form}
                    initialValues={{
                        location: "/",
                        provider: RepositoryProvider.GITEA,
                        clone: true,
                        ...(this.state.formBackup || {})
                    } as Partial<RepositoryFormData>}
                    onFinish={this.onFinish} />
            </BaseLayout>
        )
    }
}
