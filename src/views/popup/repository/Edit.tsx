import { DeleteOutlined, SaveOutlined } from '@ant-design/icons'
import { Button, notification, Typography } from 'antd'
import ButtonGroup from 'antd/lib/button/button-group'
import { FormInstance } from 'antd/lib/form'
import confirm from 'antd/lib/modal/confirm'
import React, { Component } from 'react'
import { RouteComponentProps } from 'react-router-dom'
import RepositoryForm, { RepositoryFormData } from '../../../components/repository/RepositoryForm'
import BaseLayout from '../../../layouts/BaseLayout'
import { Repository } from '../../../types'
import { messenger, RequestRepository, RequestRepositoryDelete, RequestRepositoryInit, RequestRepositoryUpdate, ResponseError, ResponseRepository, ResponseRepositoryInit } from '../../../vendors/messenger'
import { requestRepositoryInitResponseHandler } from './helpers'

type Props = RouteComponentProps<{ id: string }>;

interface State {
    item: Repository
    loading: boolean
    loadingMessage: string
}

export default class RepositoryEditView extends Component<Props, State> {
    state = {
        item: undefined,
        loading: false,
        loadingMessage: ""
    } as State;

    form = React.createRef<FormInstance<RepositoryFormData>>();


    componentDidMount = async () => {
        const { match: { params: { id } } } = this.props;

        const response: ResponseRepository = await messenger.sendMessage(new RequestRepository({ id }));

        this.setState({ item: response.body });
    }

    onDelete = () => {
        const { item: { attributes: { name } } } = this.state;

        confirm({
            title: `Sure you want to delete this repository: ${name} ?`,
            okText: "Yes, delete!",
            okType: "danger",
            cancelText: "No",
            onOk: this.delete
        });
    }

    private delete = async () => {
        const { item: { id, attributes: { name } } } = this.state;

        await messenger.sendMessage(new RequestRepositoryDelete({
            id
        }));;

        notification.success({
            message: "Deleted.",
            description: (
                <Typography.Text>
                    Repository <strong>{name}</strong> has been deleted.
                </Typography.Text>
            ),
            placement: "bottomLeft"
        });

        this.props.history.push("/repository");
    }

    onChange = (data: RepositoryFormData) => {
        console.log(data);
    }

    onFinish = async (data: RepositoryFormData) => {
        const { match: { params: { id } } } = this.props;

        this.setState({ loading: true, loadingMessage: "saving" });

        await this.save(id, data);

        if (data.clone) {
            this.setState({ loading: true, loadingMessage: "cloning" });
            await this.clone(id);
        }

        this.setState({ loading: false, loadingMessage: "" });

        this.props.history.push("/repository");
    }

    clone = async (id: string) => {
        const response = await messenger.sendMessage(new RequestRepositoryInit({ id }));
        requestRepositoryInitResponseHandler(response);
    }

    save = async (id: string, data: RepositoryFormData) => {

        await messenger.sendMessage(new RequestRepositoryUpdate({
            id,
            attributes: data
        }));

        notification.success({
            message: "Saved",
            description: `${data.name}`,
            placement: "bottomLeft"
        });
    }

    render() {
        const { item, loading, loadingMessage } = this.state;

        return (
            <BaseLayout
                loading={loading}
                loadingMessage={loadingMessage}
                pageHeader={{
                    title: "Repository",
                    subTitle: "edit",
                    onBack: () => this.props.history.push("/repository"),
                    extra: (
                        <ButtonGroup>
                            <Button
                                type="primary"
                                icon={<SaveOutlined />}
                                onClick={() => this.form.current.submit()}>
                                Save
                            </Button>
                            <Button
                                danger
                                title="Delete"
                                type="dashed"
                                icon={<DeleteOutlined />}
                                onClick={this.onDelete} />
                        </ButtonGroup>
                    )
                }}>
                {item ? (
                    <RepositoryForm
                        showSubmit={false}
                        formRef={this.form}
                        onValuesChange= {this.onChange}
                        onFinish={this.onFinish}
                        initialValues={{
                            ...item.attributes,
                            clone: false
                        }} />
                ) : null}
            </BaseLayout>
        )
    }
}
