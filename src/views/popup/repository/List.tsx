import { Button, notification } from 'antd'
import React, { Component } from 'react'
import { Link, RouteComponentProps } from 'react-router-dom'
import RepositoryList from '../../../components/repository/RepositoryList'
import BaseLayout from '../../../layouts/BaseLayout'

import PlusOutlined from "@ant-design/icons/PlusOutlined";
import { messenger, RequestRepositoryInit, ResponseError, ResponseRepositoryInit } from '../../../vendors/messenger'
import { Repository } from '../../../types'
import { requestRepositoryInitResponseHandler } from './helpers'

interface OuterProps {

}
interface State {
    loading: boolean
    loadingMessage: string
}

type Props = OuterProps & RouteComponentProps;

export default class RepositoryListView extends Component<Props, State> {
    state = {
        loading: false,
        loadingMessage: ""
    } as State;

    onClickClone = async (item: Repository) => {

        this.setState({ loading: true, loadingMessage: "Cloning" });

        const response = await messenger.sendMessage(new RequestRepositoryInit({ id: item.id }));

        requestRepositoryInitResponseHandler(response);

        this.setState({ loading: false, loadingMessage: "" });
    }

    onClickEdit = (item: Repository) => {
        this.props.history.push(`/repository/edit/${item.id}`);
    }

    render() {
        const { loading, loadingMessage } = this.state;
        return (
            <BaseLayout
                loading={loading}
                loadingMessage={loadingMessage}
                pageHeader={{
                    extra: (
                        <Link to="/repository/create">
                            <Button type="primary" icon={<PlusOutlined />}>
                                New
                            </Button>
                        </Link>
                    ),
                    title: "Repository",
                    subTitle: "list"
                }}>
                <RepositoryList onClickEdit={this.onClickEdit} onClickClone={this.onClickClone} />
            </BaseLayout>
        )
    }
}
