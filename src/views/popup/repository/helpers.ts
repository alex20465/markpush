import { notification } from "antd";
import { AbstractResponse, ResponseError, ResponseRepositoryInit } from "../../../vendors/messenger";

export function requestRepositoryInitResponseHandler(response: AbstractResponse) {
    if (response instanceof ResponseRepositoryInit && response.body === true) {
        notification.success({
            message: "Cloned",
            description: "Remote default branch cloned and stored locally (isomorphic-fs)",
            placement: "bottomLeft"
        });
    } else if (response instanceof ResponseError) {
        notification.error({
            message: response.body.name,
            description: response.body.message,
            placement: "bottomLeft"
        });
    }
}