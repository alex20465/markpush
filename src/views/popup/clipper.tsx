import { Button, notification, Result, Typography } from 'antd';
import React, { Component } from 'react'
import { RouteComponentProps } from 'react-router-dom'
import BaseLayout from '../../layouts/BaseLayout'
import ArticleForm, { ArticleFormData } from '../../components/article/ArticleForm';
import ArticleDestinationForm, { ArticleDestinationFormData } from '../../components/article/ArticleDestinationForm';
import { FormInstance } from 'antd/lib/form';
import ButtonGroup from 'antd/lib/button/button-group';
import { Article } from '../../types';
import { messenger, RequestArticle, RequestArticleImages, RequestArticleSave, RequestRepositories, RequestRepository, ResponseArticle, ResponseArticleSave, ResponseError, ResponseRepositories, ResponseRepository } from '../../vendors/messenger';
import { SettingFilled, SmileFilled, UploadOutlined } from '@ant-design/icons';

interface OuterProps {
    source: string
}
interface State {
    article?: Article
    loading: boolean
    loadingMessage: string
    hasRepositories: boolean
}

type Props = OuterProps & RouteComponentProps;

let firstVisitSingleton = true;
export default class ClipperView extends Component<Props, State> {
    state = {
        loading: true,
        loadingMessage: "",
        hasRepositories: true
    } as State;

    articleDestinationForm: React.RefObject<FormInstance> = React.createRef();

    articleForm: React.RefObject<FormInstance> = React.createRef();

    componentDidMount = async () => {
        const { source } = this.props;

        this.setState({ loading: true, loadingMessage: "Fetching" });

        const lastVisitedPathname = localStorage.getItem("last-visited");

        if (lastVisitedPathname && lastVisitedPathname !== "/" && firstVisitSingleton === true) {
            this.props.history.push(lastVisitedPathname);
        }

        firstVisitSingleton = false;

        this.props.history.listen((location) => {
            localStorage.setItem("last-visited", location.pathname)
        });

        const repositoriesRes: ResponseRepositories = await messenger.sendMessage(new RequestRepositories());

        const articleRes = await messenger.sendMessage(new RequestArticle({
            url: source,
            downloadImages: false
        }));

        if (articleRes instanceof ResponseArticle) {
            this.setState({
                loading: false,
                loadingMessage: "",
                hasRepositories: repositoriesRes.body.length > 0,
                article: articleRes.body
            });
        } else if (articleRes instanceof ResponseError) {
            this.setState({
                loading: false,
                loadingMessage: "",
                article: null
            });
        }
    }

    getInitialRepositoryDestinationValues = (): Partial<ArticleDestinationFormData> => {
        const lastSelected = localStorage.getItem('last-selected-destination');

        if (lastSelected === null) {
            return {};
        } else {
            return {
                repository: lastSelected
            }
        }
    }

    fetchArticleImages = async (article: Article, repositoryId: string) => {
        await messenger.sendMessage(new RequestArticleImages({
            article,
            repositoryId
        }));
    }

    onSubmit = async () => {

        const {
            articleDestinationForm: {
                current: articleDestinationForm
            },
            articleForm: {
                current: articleForm
            }
        } = this;

        let { article } = this.state;

        const destinationData = (await articleDestinationForm.validateFields() as ArticleDestinationFormData);
        const { source, title } = (await articleForm.validateFields() as ArticleFormData);

        localStorage.setItem('last-selected-destination', destinationData.repository);

        const repository = await messenger.sendMessage(new RequestRepository({
            id: destinationData.repository
        })) as ResponseRepository

        if (repository.body.attributes.downloadImages) {
            this.setState({ loading: true, loadingMessage: "Fetch Images" });
            // Re-download article with local-image-references.
            const res: ResponseArticle = await messenger.sendMessage(new RequestArticle({
                url: article.source,
                downloadImages: true
            }));

            article = res.body;

            await this.fetchArticleImages(article, repository.body.id);
        }

        this.setState({ loading: true, loadingMessage: "Pushing" });

        const response = await messenger.sendMessage(new RequestArticleSave({
            article: {
                ...article,
                title,
                source
            },
            repositoryId: destinationData.repository
        }));

        if (response instanceof ResponseArticleSave && response.body === true) {
            notification.success({
                message: "Pushed",
                description: "Article committed and pushed under the default remote branch.",
                placement: "bottomLeft"
            });
        } else if (response instanceof ResponseError) {
            notification.error({
                message: response.body.name,
                description: response.body.message,
                placement: "bottomLeft"
            })
        } else {
            notification.error({
                message: "Error",
                description: "A unexpected error occurred",
                placement: "bottomLeft"
            });
        }
        this.setState({ loading: false, loadingMessage: "" });
    }

    renderError = () => {
        return (
            <Result
                status="error"
                title="@mozilla/readability"
                subTitle="Sorry, the library we use from Mozilla can't read this page."
                extra={(
                    <Button
                        onClick={() => notification.info({ message: "Hello !", icon: (<SmileFilled />), placement: "bottomLeft" })}
                        type="primary">
                        Say Hi !
                    </Button>
                )}
            />
        )
    }

    renderNoRepositories = () => {
        return (
            <Result
                icon={<SettingFilled />}
                status="info"
                title="Setup"
                subTitle={(
                    <Typography.Text>
                        Lets getting started and setup your first repository !!
                    </Typography.Text>
                )}
                extra={(
                    <Button
                        onClick={() => this.props.history.push("/repository/create")}
                        type="primary">
                        Create
                    </Button>
                )}
            />
        )
    }

    renderForms = () => {
        const { article, loading } = this.state;

        return (
            <React.Fragment>
                <ArticleDestinationForm
                    renderSubmit={false}
                    initialValues={this.getInitialRepositoryDestinationValues()}
                    formRef={this.articleDestinationForm}
                />
                <ArticleForm
                    loading={loading}
                    renderSubmit={false}
                    formRef={this.articleForm}
                    initialValues={article || {}} />
            </React.Fragment>
        );
    }

    render() {
        const { article, loading, loadingMessage, hasRepositories } = this.state;

        let content;

        if (article === null && loading === false) {
            content = this.renderError();
        } else if (hasRepositories === false) {
            content = this.renderNoRepositories();
        } else {
            content = this.renderForms();
        }

        return (
            <BaseLayout
                loading={loading}
                loadingMessage={loadingMessage}
                pageHeader={{
                    title: "Article",
                    subTitle: "push",
                    extra: (
                        <ButtonGroup>
                            <Button disabled={!article || !hasRepositories} type="primary" onClick={this.onSubmit}>
                                <UploadOutlined /> Push
                            </Button>
                        </ButtonGroup>
                    )
                }}>
                {content}
            </BaseLayout>
        )
    }
}
