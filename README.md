# Mark PUSH

**MarkPUSH** is a chrome and firefox extension which allows users to push website articles on a git based version system in a readable format (markdown).

## Download (store)

- [Firefox](https://addons.mozilla.org/en-US/firefox/addon/markpush/)
- [Google Chrome](https://chrome.google.com/webstore/detail/markpush/ocljocjeffmobjichfggfdbiihoipfgb)

## NOTE

IS CURRENTLY UNDER DEVELOPMENT, NOT RELEASED YET.

## Features

- Configure multiple repositories
- Use same repository with a different article-base location.

## Implementation

It uses `@mozilla/readability` and `turndown` to generate the markdown content, next up we use `@isomorphic-git/lightning-fs` and `isomorphic-git` to `initialize`, `add` and `commit` the change into the repository. Finally we just `PUSH` the changes.
